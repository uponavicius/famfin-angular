import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {IncomesComponent} from './components/incomes/incomes.component';
import {ExpensesComponent} from './components/expenses/expenses.component';
import {StatisticsComponent} from './components/statistics/statistics.component';
import {AccountSettingsComponent} from './components/account-settings/account-settings.component';
import {TypesSettingsComponent} from './components/account-settings/types-settings/types-settings.component';
import {BalanceComponent} from './components/statistics/balance/balance.component';
import {IncomesStatisticComponent} from './components/statistics/incomes-statistic/incomes-statistic.component';
import {ExpensesStatisticComponent} from './components/statistics/expenses-statistic/expenses-statistic.component';
import {GeneralSettingsComponent} from './components/account-settings/general-settings/general-settings.component';
import {LoginComponent} from "./components/auth/login/login.component";
import {RegisterComponent} from "./components/auth/register/register.component";
import {LogoutComponent} from "./components/auth/logout/logout.component";
import {AuthGuardService} from "./services/auth-guard.service";

const appRoutes: Routes = [
  {path: '', redirectTo: 'expenses', pathMatch: 'full', canActivate:[AuthGuardService]},
  {path: 'incomes', component: IncomesComponent, canActivate: [AuthGuardService]},
  {path: 'expenses', component: ExpensesComponent, canActivate: [AuthGuardService]},
  {
    path: 'statistic', component: StatisticsComponent, canActivate:[AuthGuardService], children: [
      {path: '', component: BalanceComponent, canActivate:[AuthGuardService]},
      {path: 'balance', component: BalanceComponent, canActivate:[AuthGuardService]},
      {path: 'incomes-statistic', component: IncomesStatisticComponent, canActivate:[AuthGuardService]},
      {path: 'expenses-statistic', component: ExpensesStatisticComponent, canActivate:[AuthGuardService]},
    ]
  },
  {
    path: 'account-settings', component: AccountSettingsComponent, canActivate:[AuthGuardService], children: [
      {path: '', component: GeneralSettingsComponent, canActivate:[AuthGuardService]},
      {path: 'general-settings', component: GeneralSettingsComponent, canActivate:[AuthGuardService]},
      {path: 'types-settings', component: TypesSettingsComponent, canActivate:[AuthGuardService]}
    ]
  },
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LogoutComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
