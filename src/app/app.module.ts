import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {HeaderComponent} from './components/header/header.component';
import {IncomesComponent} from './components/incomes/incomes.component';
import {ExpensesComponent} from './components/expenses/expenses.component';
import {StatisticsComponent} from './components/statistics/statistics.component';
import {NewIncomeComponent} from './components/incomes/new-income/new-income.component';
import {NewExpenseComponent} from './components/expenses/new-expense/new-expense.component';
import {ExpensesListComponent} from './components/expenses/expenses-list/expenses-list.component';
import {IncomesListComponent} from './components/incomes/incomes-list/incomes-list.component';
import {IncomesStatisticComponent} from './components/statistics/incomes-statistic/incomes-statistic.component';
import {ExpensesStatisticComponent} from './components/statistics/expenses-statistic/expenses-statistic.component';
import {BalanceComponent} from './components/statistics/balance/balance.component';
import {AccountSettingsComponent} from './components/account-settings/account-settings.component';
import {GeneralSettingsComponent} from './components/account-settings/general-settings/general-settings.component';
import {TypesSettingsComponent} from './components/account-settings/types-settings/types-settings.component';
import {CurrentMonthStatisticComponent} from './components/statistics/current-month-statistic/current-month-statistic.component';
import {AuthComponent} from './components/auth/auth.component';
import {LoginComponent} from './components/auth/login/login.component';
import {RegisterComponent} from './components/auth/register/register.component';
import {FooterComponent} from './components/footer/footer.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {LogoutComponent} from './components/auth/logout/logout.component';
import {HttpInterceptorService} from "./services/http-interceptor.service";
import {ChartsModule} from 'ng2-charts';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    IncomesComponent,
    ExpensesComponent,
    StatisticsComponent,
    NewIncomeComponent,
    NewExpenseComponent,
    ExpensesListComponent,
    IncomesListComponent,
    IncomesStatisticComponent,
    ExpensesStatisticComponent,
    BalanceComponent,
    AccountSettingsComponent,
    GeneralSettingsComponent,
    TypesSettingsComponent,
    CurrentMonthStatisticComponent,
    AuthComponent,
    LoginComponent,
    RegisterComponent,
    FooterComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgbModule,
    AppRoutingModule,
    HttpClientModule,
    ChartsModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
