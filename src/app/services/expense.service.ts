import {Injectable} from '@angular/core';
import {ExpenseModel} from "../models/expense.model";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {OperationStatusModel} from "../models/operation-status.model";

@Injectable({
  providedIn: 'root'
})
export class ExpenseService {
  url = 'http://localhost:8080/app-ws/users/';
  userId = sessionStorage.getItem('UserId');
  expenseTypeUri = '/expenseType/';
  expenseUri = '/expense/';
  expensesUri = '/expenses';
  dateFromUri = "?dateFrom=";
  dateToUri = "&dateTo=";

  constructor(private http: HttpClient) {
  }

  createNewExpense(expenseTypeId: string, date: string, name: string, amount: number) {
    return this.http.post<ExpenseModel>(
      this.url + this.userId + this.expenseTypeUri + expenseTypeId + this.expenseUri,
      {date, name, amount}
    );
  }

  // TODO blogai veikia, nes reikia einamo menesio
  // http://localhost:8080/app-ws/users/rFQvVnRMkbtDrSCJ15xffMZqoqjjQw/expenses?dateFrom=2020-01-01&dateTo=2020-06-16
  getExpensesBetweenTwoDays(dateFrom: string, dateTo: string): Observable<ExpenseModel[]> {
    return this.http.get<ExpenseModel[]>(
      this.url + this.userId + this.expensesUri + this.dateFromUri + dateFrom + this.dateToUri + dateTo
    );
  }

  updateExpense(expenseTypeId: string, expenseId: string, date: string, name: string, amount: number) {
    return this.http.put<ExpenseModel>(
      this.url + this.url + this.expenseTypeUri + expenseTypeId + this.expenseUri + expenseId,
      {date, name, amount}
    );
  }

  deleteIncome(expenseTypeId: string, expenseId: string) {
    return this.http.delete<OperationStatusModel>(
      this.url + this.userId + this.expenseTypeUri + expenseTypeId + this.expenseUri + expenseId
    );
  }


}
