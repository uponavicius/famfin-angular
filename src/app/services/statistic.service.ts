import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {StatisticModel} from "../models/statistic.model";

@Injectable({
  providedIn: 'root'
})
export class StatisticService {
  url = 'http://localhost:8080/app-ws/users/';
  userId = sessionStorage.getItem('UserId');
  sharedStatisticURI = '/sharedStatistic';
  balanceURI = '/balance';
  incomesStatisticURI = '/incomesStatistic';
  expenseStatisticURI = '/expenseStatistic';

  dateFromURi = '?dateFrom='
  dateToURI = '&dateTo='


  constructor(private http: HttpClient) {
  }

  getCurrentMonthStatistic(): Observable<StatisticModel> {
    return this.http.get<StatisticModel>(this.url + this.userId + this.sharedStatisticURI);
  }

  getBalance(dateFrom: string, dateTo: string): Observable<StatisticModel> {
    return this.http.get<StatisticModel>(
      this.url + this.userId + this.balanceURI + this.dateFromURi + dateFrom + this.dateToURI + dateTo
    );
  }

  getIncomeStatistic(dateFrom: string, dateTo: string): Observable<StatisticModel> {
    return this.http.get<StatisticModel>(
      this.url + this.userId + this.incomesStatisticURI + this.dateFromURi + dateFrom + this.dateToURI + dateTo
    );
  }

  getExpenseStatistic(dateFrom: string, dateTo: string): Observable<StatisticModel> {
    return this.http.get<StatisticModel>(
      this.url + this.userId + this.expenseStatisticURI + this.dateFromURi + dateFrom + this.dateToURI + dateTo
    );
  }
}


