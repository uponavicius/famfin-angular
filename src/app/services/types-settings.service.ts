import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {IncomeTypeModel} from "../models/income-type.model";
import {Observable} from "rxjs";
import {OperationStatusModel} from "../models/operation-status.model";
import {ExpenseTypeModel} from "../models/expense-type.model";

@Injectable({
  providedIn: 'root'
})
export class TypesSettingsService {
  url = 'http://localhost:8080/app-ws/users/';
  userId = sessionStorage.getItem('UserId');
  incomeTypeURI = '/incomeType/';
  expenseTypeURI = '/expenseType/';


  constructor(private http: HttpClient) {
  }

  createIncomeType(incomeTypeName: string) {
    return this.http.post<IncomeTypeModel>(this.url + this.userId + this.incomeTypeURI, {incomeTypeName});
  }

  getIncomeTypes(): Observable<IncomeTypeModel[]> {
    return this.http.get<IncomeTypeModel[]>(this.url + this.userId + this.incomeTypeURI);
  }

  updateIncomeType(incomeTypeName: string, incomeTypeId: string) {
    return this.http.put<IncomeTypeModel>(this.url + this.userId + this.incomeTypeURI + incomeTypeId, {incomeTypeName});
  }

  deleteIncomeType(incomeTypeId: string) {
    return this.http.delete<OperationStatusModel>(this.url + this.userId + this.incomeTypeURI + incomeTypeId);
  }

  createExpenseType(expenseTypeName: string) {
    return this.http.post<ExpenseTypeModel>(this.url + this.userId + this.expenseTypeURI, {expenseTypeName});
  }

  getExpenseTypes(): Observable<ExpenseTypeModel[]> {
    return this.http.get<ExpenseTypeModel[]>(this.url + this.userId + this.expenseTypeURI);
  }

  updateExpenseType(expenseTypeName: string, expenseTypeId: string) {
    return this.http.put<ExpenseTypeModel>(this.url + this.userId + this.expenseTypeURI + expenseTypeId, {expenseTypeName});
  }

  deleteExpenseType(expenseTypeId: string) {
    return this.http.delete<OperationStatusModel>(this.url + this.userId + this.expenseTypeURI + expenseTypeId)
  }
}
