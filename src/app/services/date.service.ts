import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateService {
  private currentDay: string[] = new Date().toISOString().split('-');
  private year = this.currentDay[0];
  private month = this.currentDay[1];
  private day = this.currentDay[2];
  private lastMonthDay = new Date(+this.year, +this.month, 0).getDate();


  constructor() {
  }

  today() {
    return new Date().toISOString().slice(0, 10);
  }

  lastYearCurrentMonthFirstDay() {
    return ((+this.year - 1) + '-' + this.month + '-01').toString();
  }

  currentMonthFirstDay() {
    return (this.year + '-' + this.month + '-01').toString();
  }

  currentMonthLastDay() {
    return (this.year + '-' + this.month + '-' + this.lastMonthDay).toString();
  }

}
