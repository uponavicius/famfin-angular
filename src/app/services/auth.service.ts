import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse, HttpHeaders} from "@angular/common/http";
import {map} from "rxjs/operators";
import {Observable, Subscription} from "rxjs";
import {Config} from "codelyzer";
import {UserModel} from "../models/user.model";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  url = 'http://localhost:8080/app-ws/users/';

  constructor(private http: HttpClient) {
  }


  authenticate(email: string, password: string): Observable<HttpResponse<Config>> {
    return this.http.post<Config>(
      this.url + 'login',
      {email, password},
      {observe: 'response'});
  }


  isUserLoggedIn() {
    return (sessionStorage.getItem('Authorization') !== null && sessionStorage.getItem('UserId') !== null);
  }

  logOut() {
    sessionStorage.removeItem('Authorization')
    sessionStorage.removeItem('UserId')
    sessionStorage.removeItem('FirstName')
  }

  createUser(firstName: string, lastName: string, email: string, password: string) {
    return this.http.post<UserModel>('http://localhost:8080/app-ws/users', {firstName, lastName, email, password})
  }
}
