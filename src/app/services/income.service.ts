import {Injectable} from '@angular/core';
import {IncomeModel} from "../models/income.model";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {OperationStatusModel} from "../models/operation-status.model";

@Injectable({
  providedIn: 'root'
})
export class IncomeService {

  url = 'http://localhost:8080/app-ws/users/';
  userId = sessionStorage.getItem('UserId');
  incomeTypeUri = '/incomeType/';
  incomeUri = '/income/';
  incomesUri = '/incomes';
  dateFromUri = "?dateFrom=";
  dateToUri = "&dateTo=";


  constructor(private http: HttpClient) {
  }

  createNewIncome(incomeTypeId: string, date: string, name: string, amount: number) {
    return this.http.post<IncomeModel>(
      this.url + this.userId + this.incomeTypeUri + incomeTypeId + this.incomeUri,
      {date, name, amount}
    );
  }

  getIncomesBetweenTwoDays(dateFrom: string, dateTo: string): Observable<IncomeModel[]> {
    return this.http.get<IncomeModel[]>(
      this.url + this.userId + this.incomesUri + this.dateFromUri + dateFrom + this.dateToUri + dateTo
    );
  }

  updateIncome(incomeTypeId: string, incomeId: string, date: string, name: string, amount: number) {
    return this.http.put<IncomeModel>(
      this.url + this.userId + this.incomeTypeUri + incomeTypeId + this.incomeUri + incomeId,
      {date, name, amount}
    );
  }

  deleteIncome(incomeTypeId: string, incomeId: string) {
    return this.http.delete<OperationStatusModel>(
      this.url + this.userId + this.incomeTypeUri + incomeTypeId + this.incomeUri + incomeId
    );
  }


}
