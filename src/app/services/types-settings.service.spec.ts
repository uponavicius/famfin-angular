import { TestBed } from '@angular/core/testing';

import { TypesSettingsService } from './types-settings.service';

describe('TypesSettingsService', () => {
  let service: TypesSettingsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TypesSettingsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
