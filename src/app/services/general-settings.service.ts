import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {OperationStatusModel} from "../models/operation-status.model";
import {AuthService} from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export class GeneralSettingsService {

  url = 'http://localhost:8080/app-ws/users/';
  userId = sessionStorage.getItem('UserId');

  constructor(private http: HttpClient) {
  }

  // http://localhost:8080/app-ws/users/8gQFTbGrxI7KJEffqG7zYgA3kyEBjr
  deleteAccount() {
    return this.http.delete<OperationStatusModel>(this.url + this.userId);


  }
}
