import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypesSettingsComponent } from './types-settings.component';

describe('TypesSettingsComponent', () => {
  let component: TypesSettingsComponent;
  let fixture: ComponentFixture<TypesSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypesSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypesSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
