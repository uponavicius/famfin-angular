import {Component, OnInit, Output, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {ExpenseTypeModel} from "../../../models/expense-type.model";
import {IncomeTypeModel} from "../../../models/income-type.model";
import {TypesSettingsService} from "../../../services/types-settings.service";

@Component({
  selector: 'app-types-settings',
  templateUrl: './types-settings.component.html',
  styleUrls: ['./types-settings.component.css']
})
export class TypesSettingsComponent implements OnInit {
  @ViewChild('fIncomeType') incomeTypeForm: NgForm;
  @ViewChild('fExpenseType') expenseTypeForm: NgForm;

  expenseTypes: ExpenseTypeModel[];
  incomeTypes: IncomeTypeModel[];
  incomeTypeMessage: string;
  expenseTypeMessage: string;

  constructor(private typesSettingsService: TypesSettingsService) {
  }

  ngOnInit(): void {
    this.getExpenseTypes();
    this.getIncomeTypes();
  }

  onIncomeTypeCreate() {
    const incomeTypeName = this.incomeTypeForm.value.incomeTypeCreate.incomeTypeName
    if (incomeTypeName !== '') {
      this
        .typesSettingsService
        .createIncomeType(incomeTypeName)
        .subscribe(resp => {
          if (incomeTypeName === resp.incomeTypeName) {
            this.getIncomeTypes();
            this.incomeTypeForm.reset();
          }
          // TODO pagauti klaida - kai kuriamas tipas toks koks jau yra
        });
    } else {
      this.incomeTypeMessage = 'Please enter new income type name.'
    }
  }

  getIncomeTypes() {
    this.typesSettingsService.getIncomeTypes().subscribe(resp => {
      this.incomeTypes = resp.sort((a, b) => a.incomeTypeName.localeCompare(b.incomeTypeName));
    })
  }

  onIncomeTypeUpdate(newName: any, typeId: string) {
    if (newName !== '') {
      this
        .typesSettingsService
        .updateIncomeType(newName, typeId)
        .subscribe(resp => {
          if (newName === resp.incomeTypeName) {
            this.getIncomeTypes();
          } else {
            console.log("pagauti klaida income update")
            // TODO pagauti klaida ir isvesti i weba
          }
        })
    } else {
      this.incomeTypeMessage = 'Income type name can not be an empty field.'
    }
  }

  onIncomeTypeDelete(incomeTypeId: string) {
    this
      .typesSettingsService
      .deleteIncomeType(incomeTypeId)
      .subscribe(resp => {
        if (resp.operationResult === 'SUCCESS') {
          this.getIncomeTypes();
          // this.incomeTypeMessage = 'Income type deleted successfully';
          // this.hideElement('incomeTypeMessageId');
        } else {
          // TODO pagauti klaida
        }
      })

  }

  onExpenseTypeCreate() {
    const expenseTypeName = this.expenseTypeForm.value.expenseTypeCreate.expenseTypeName;
    if (expenseTypeName !== '') {
      this
        .typesSettingsService
        .createExpenseType(expenseTypeName)
        .subscribe(resp => {
          if ((expenseTypeName === resp.expenseTypeName)) {
            this.getExpenseTypes()
            this.expenseTypeForm.reset();
          }
          // TODO pagauti klaida - kai kuriamas tipas toks koks jau yra
        })
    } else {
      this.expenseTypeMessage = 'Please enter new expense type name.' //TODO kazkodel ismeta sita pavadinima kai update expende
    }

  }

  getExpenseTypes() {
    this.typesSettingsService.getExpenseTypes().subscribe(resp => {
      this.expenseTypes = resp.sort((a, b) => a.expenseTypeName.localeCompare(b.expenseTypeName));
    })
  }

  onExpenseTypeUpdate(newName: any, typeId: string) {
    if (newName !== '') {
      this
        .typesSettingsService
        .updateExpenseType(newName, typeId)
        .subscribe(resp => {
          if (newName === resp.expenseTypeName) {
            console.log("expenseType update ok")
            this.getExpenseTypes();
          } else {
            console.log("pagauti klaida expense update")
            // TODO pagauti klaida ir isvesti i weba
          }
        })
    } else {
      this.expenseTypeMessage = 'Expense type name can not be an empty field.'
    }

  }

  onExpenseTypeDelete(expenseTypeId: string) {
    this
      .typesSettingsService
      .deleteExpenseType(expenseTypeId)
      .subscribe(resp => {
        if (resp.operationResult === 'SUCCESS') {
          this.getExpenseTypes();
          // this.expenseTypeMessage = 'Expense type deleted successfully';
        } else {
          // TODO pagauti klaida
        }
      })
  }
}
