import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthService} from "../../../services/auth.service";
import {GeneralSettingsService} from "../../../services/general-settings.service";
import {ok} from "assert";
import {Router} from "@angular/router";

@Component({
  selector: 'app-general-settings',
  templateUrl: './general-settings.component.html',
  styleUrls: ['./general-settings.component.css']
})
export class GeneralSettingsComponent implements OnInit {
  @ViewChild('fChangePass') changePassForm: NgForm;
  @ViewChild('fDeleteAccount') deleteAccountForm: NgForm;
  message: string;

  constructor(private authService: AuthService,
              private generalSettingsService: GeneralSettingsService,
              private router: Router) {
  }

  ngOnInit(): void {
  }

  onChangePasswordSubmit() {
    console.log(this.changePassForm);
    this.changePassForm.reset();
  }

  onDeleteAccountSubmit(email: string, password: string) {
    this.authService.authenticate(email, password).subscribe(resp => {
      if (sessionStorage.getItem('UserId') === resp.headers.get("UserId")) {
        this.generalSettingsService.deleteAccount().subscribe(deleteResp => {
          if (deleteResp.operationResult === 'SUCCESS') {
            sessionStorage.removeItem('Authorization')
            sessionStorage.removeItem('UserId')
            sessionStorage.removeItem('FirstName')

            this.authService.logOut();
            this.router.navigate(['login']);

            alert('Your account and all data has been deleted!')
          }
        })

      } else {
        // TODO neveikia si dalis. pagauti klaida kitaip
        this.message = 'Incorrect email or password';
        console.log('Incorrect email or password')
      }
    })

    // logintis
    // patinkrinti ar storage yra tas pats id
    // jeigu tas pats id, tai tada trinti useri
    // ismesti alerta, kad useris ir visi duomenys istrinti
    // jeigu success, tai tada isloginti

  }
}
