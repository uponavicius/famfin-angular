import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncomesStatisticComponent } from './incomes-statistic.component';

describe('IncomesStatisticComponent', () => {
  let component: IncomesStatisticComponent;
  let fixture: ComponentFixture<IncomesStatisticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncomesStatisticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncomesStatisticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
