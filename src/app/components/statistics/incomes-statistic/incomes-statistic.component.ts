import {Component, OnInit, ViewChild} from '@angular/core';
import {DateService} from '../../../services/date.service';
import {NgForm} from '@angular/forms';
import {ChartOptions, ChartType} from 'chart.js';
import {Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip, SingleDataSet} from 'ng2-charts';
import {StatisticService} from "../../../services/statistic.service";

@Component({
  selector: 'app-incomes-statistic',
  templateUrl: './incomes-statistic.component.html',
  styleUrls: ['./incomes-statistic.component.css']
})
export class IncomesStatisticComponent implements OnInit {
  @ViewChild('f') incomeStatisticForm: NgForm;
  todayDate = new DateService().today();
  defaultFirstMonthDay = new DateService().currentMonthFirstDay();
  incomesStatistic: any[] = []
  amountSum: number = 0;


  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[] = [];
  public pieChartData: SingleDataSet = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public chartColors: any[] = [
    {
      backgroundColor: [
        "#ff4444", "#ffbb33", "#00C851", "#aa66cc", "#00695c", "#00695c",
        "#33b5e5", "#FF8800", "#007E33", "#0d47a1", "#9933CC", "#c0ca33",
        "#0099CC", "#2BBBAD", "#4285F4", "#e65100", "#00695c", "#ff9800"
      ]
    }];

  constructor(private statisticService: StatisticService) {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }

  ngOnInit(): void {
    this.onSubmit(this.defaultFirstMonthDay, this.todayDate);
  }

  // TODO siandienos dienos neima, jeigu laikas yra ne 00:00:00
  onSubmit(dateFrom: string, dateTo: string) {
    this.statisticService.getIncomeStatistic(dateFrom, dateTo).subscribe(resp => {
      if (resp !== null) {
        this.incomesStatistic = resp.incomesStatistic;
        this.pieChartLabels = Object.keys(resp.incomesStatistic);
        this.pieChartData = Object.values(resp.incomesStatistic);

        let sum: number = 0
        for (let val of Object.values(resp.incomesStatistic)) {
          sum += val;
        }
        this.amountSum = sum;
      }
    })
  }

}
