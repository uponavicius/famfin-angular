import {Component, OnInit} from '@angular/core';
import {IncomeService} from "../../../services/income.service";
import {IncomeModel} from "../../../models/income.model";
import {StatisticService} from "../../../services/statistic.service";
import {StatisticModel} from "../../../models/statistic.model";

@Component({
  selector: 'app-current-month-statistic',
  templateUrl: './current-month-statistic.component.html',
  styleUrls: ['./current-month-statistic.component.css']
})
export class CurrentMonthStatisticComponent implements OnInit {
  sharedStatistic: StatisticModel;

  constructor(private statisticService: StatisticService) {
  }

  ngOnInit(): void {
    this.getCurrentMonthStatistic();
  }

  getCurrentMonthStatistic() {
    this.statisticService.getCurrentMonthStatistic().subscribe(resp => {
      this.sharedStatistic = resp;
    });
  }

  updateInfo() {
    this.getCurrentMonthStatistic();
  }

}
