import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentMonthStatisticComponent } from './current-month-statistic.component';

describe('CurrentMonthStatisticComponent', () => {
  let component: CurrentMonthStatisticComponent;
  let fixture: ComponentFixture<CurrentMonthStatisticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentMonthStatisticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentMonthStatisticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
