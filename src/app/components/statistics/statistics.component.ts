import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {
  // @Output() featureSelected = new EventEmitter<string>();
  // @Input() featureSelected = new EventEmitter<string>();
  featureSelected = new EventEmitter<string>();


  constructor() {
  }

  ngOnInit(): void {
  }

  onSelect(feature: string) {
    this.featureSelected.emit(feature);
  }


}
