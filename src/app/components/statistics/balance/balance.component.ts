import {AfterContentInit, Component, DoCheck, OnChanges, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';

import {DateService} from '../../../services/date.service';
import {ChartDataSets} from 'chart.js';
import {Color, Label} from 'ng2-charts';
import {StatisticService} from "../../../services/statistic.service";


@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.css']
})
export class BalanceComponent implements OnInit {
  @ViewChild('f') balanceForm: NgForm;

  todayDate = new DateService().today();
  defaultLastYearCurrentMonthFirstDay = new DateService().lastYearCurrentMonthFirstDay();
  balanceDate: any[] = []
  incomes: number[] = [];
  expenses: number[] = [];
  incomesAverage: number[] = [];
  expensesAverage: number[] = [];
  incomeAverage:number; // TODO jei nepanaudosiu - istrinti
  expenseAverage:number;


  lineChartData: ChartDataSets[] = [
    {data: this.incomes, label: ''},
    {data: this.expenses, label: ''}
  ];

  lineChartLabels: Label[] = []; // months

  lineChartOptions = {
    responsive: true,
  };

  lineChartColors: Color[] = [
    {
      borderColor: '#ff4444',
      backgroundColor: 'rgba(255, 68, 68,0.28)',
    },
    {
      borderColor: '#0d47a1',
      backgroundColor: 'rgba(13, 71, 161,0.28)',
    },
    {
      borderColor: '#33b5e5',
      backgroundColor: 'rgba(255,255,255,0.28)',
    },
    {
      borderColor: '#007E33',
      backgroundColor: 'rgba(255,255,255,0.28)',
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';

  constructor(private statisticService: StatisticService) {
  }

  ngOnInit(): void {
    this.onSubmit(this.defaultLastYearCurrentMonthFirstDay, this.todayDate);
  }

  onSubmit(dateFrom: string, dateTo: string) {
    this.statisticService.getBalance(dateFrom, dateTo).subscribe(resp => {
      if (resp !== null) {
        this.lineChartLabels = Object.keys(resp.expensesByMonth);
        this.incomes = Object.values(resp.incomesByMonth);
        this.expenses = Object.values(resp.expensesByMonth);

        let expenseSum: number = 0
        let count: number = 0
        for (let val of Object.values(resp.expensesByMonth)) {
          expenseSum += val;
          count++;
        }
        let expenseAvg: number = expenseSum / count;
        this.expenseAverage = expenseAvg;

        let incomeSum: number = 0;
        for (let val of Object.values(resp.incomesByMonth)) {
          incomeSum += val;
        }
        let incomeAvg: number = incomeSum / count;
        this.incomeAverage = incomeAvg;

        for (let i = 0; i < count; i++) {
          this.expensesAverage.push(expenseAvg);
          this.incomesAverage.push(incomeAvg);
        }


        this.balanceDate.push(Object.values(resp.incomesByMonth));
        this.balanceDate.push(Object.values(resp.expensesByMonth));

        this.lineChartData = [
          {data: this.incomes, label: 'Incomes'},
          {data: this.expenses, label: 'Expenses'},
          {data: this.incomesAverage, label: 'Incomes Average'},
          {data: this.expensesAverage, label: 'Expenses Average'},
        ];

      }
    })

  }


}
