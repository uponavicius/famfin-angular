import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpensesStatisticComponent } from './expenses-statistic.component';

describe('ExpensesStatisticComponent', () => {
  let component: ExpensesStatisticComponent;
  let fixture: ComponentFixture<ExpensesStatisticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpensesStatisticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpensesStatisticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
