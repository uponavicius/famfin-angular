import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {DateService} from '../../../services/date.service';
import {ChartOptions, ChartType} from 'chart.js';
import {Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip, SingleDataSet} from 'ng2-charts';
import {StatisticService} from "../../../services/statistic.service";

@Component({
  selector: 'app-expenses-statistic',
  templateUrl: './expenses-statistic.component.html',
  styleUrls: ['./expenses-statistic.component.css']
})
export class ExpensesStatisticComponent implements OnInit {
  @ViewChild('f') expenseStatisticForm: NgForm;
  todayDate = new DateService().today();
  defaultFirstMonthDay = new DateService().currentMonthFirstDay();
  expenseStatistic: any[] = []
  amountSum: number = 0;

  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[] = [];
  public pieChartData: SingleDataSet = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public chartColors: any[] = [
    {
      backgroundColor: [
        "#ff4444", "#ffbb33", "#00C851", "#aa66cc", "#00695c", "#00695c",
        "#33b5e5", "#FF8800", "#007E33", "#0d47a1", "#9933CC", "#c0ca33",
        "#0099CC", "#2BBBAD", "#4285F4", "#e65100", "#00695c", "#ff9800"
      ]
    }];


  constructor(private statisticService: StatisticService) {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }

  ngOnInit(): void {
    this.onSubmit(this.defaultFirstMonthDay, this.todayDate);
  }

  // TODO siandienos dienos neima, jeigu laikas yra ne 00:00:00
  onSubmit(dateFrom: string, dateTo: string) {
    this.statisticService.getExpenseStatistic(dateFrom, dateTo).subscribe(resp => {
      if (resp !== null) {
        this.expenseStatistic = resp.expenseStatistic;
        this.pieChartLabels = Object.keys(resp.expenseStatistic);
        this.pieChartData = Object.values(resp.expenseStatistic);

        let sum: number = 0
        for (let val of Object.values(resp.expenseStatistic)) {
          sum += val;
        }
        this.amountSum = sum;
      }
    })
  }

}
