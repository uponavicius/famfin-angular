import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {DateService} from '../../../services/date.service';
import {TypesSettingsService} from "../../../services/types-settings.service";
import {IncomeTypeModel} from "../../../models/income-type.model";
import {IncomeService} from "../../../services/income.service";

@Component({
  selector: 'app-new-income',
  templateUrl: './new-income.component.html',
  styleUrls: ['./new-income.component.css']
})
export class NewIncomeComponent implements OnInit {
  @ViewChild('f') newIncomeForm: NgForm;
  todayDate = new DateService().today();
  @Input() incomeTypes: IncomeTypeModel[];

  @Output() infoChanged: EventEmitter<void> = new EventEmitter();

  constructor(private typesSettingsService: TypesSettingsService,
              private incomeService: IncomeService) {
  }

  ngOnInit(): void {
    this.getIncomeTypes();
  }

  onSubmit(typeName: any) {
    const date = this.newIncomeForm.value.newIncomeRecord.date;
    const name = this.newIncomeForm.value.newIncomeRecord.name;
    const amount = this.newIncomeForm.value.newIncomeRecord.value

    this.incomeService.createNewIncome(this.findIncomeTypeId(typeName), date, name, amount)
      .subscribe(resp => {
        if (resp.name === name) {
          this.infoChanged.emit();
        }
      })
  }

  getIncomeTypes() {
    this.typesSettingsService.getIncomeTypes().subscribe(resp => {
      this.incomeTypes = resp.sort((a, b) => a.incomeTypeName.localeCompare(b.incomeTypeName));
    })
  }

  findIncomeTypeId(name: string) {
    return this.incomeTypes.find(i => i.incomeTypeName === name).incomeTypeId;
  }



}
