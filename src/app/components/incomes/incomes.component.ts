import {Component, OnInit, ViewChild} from '@angular/core';
import {IncomesListComponent} from "./incomes-list/incomes-list.component";
import {CurrentMonthStatisticComponent} from "../statistics/current-month-statistic/current-month-statistic.component";

@Component({
  selector: 'app-incomes',
  templateUrl: './incomes.component.html',
  styleUrls: ['./incomes.component.css']
})
export class IncomesComponent implements OnInit {
  // isChangedComponent: boolean;
  @ViewChild(IncomesListComponent) incomesListComponent:IncomesListComponent;
  @ViewChild(CurrentMonthStatisticComponent) currentMonthStatisticComponent:CurrentMonthStatisticComponent;

  constructor() {
  }

  ngOnInit(): void {
  }

  reloadAnotherComponent() {
    // this.isChangedComponent = true;
    this.incomesListComponent.updateInfo();
    this.currentMonthStatisticComponent.updateInfo();
  }


}
