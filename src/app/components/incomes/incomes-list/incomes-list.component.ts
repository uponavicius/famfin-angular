import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {IncomeService} from "../../../services/income.service";
import {IncomeModel} from "../../../models/income.model";
import {TypesSettingsService} from "../../../services/types-settings.service";
import {IncomeTypeModel} from "../../../models/income-type.model";
import {DateService} from "../../../services/date.service";


@Component({
  selector: 'app-incomes-list',
  templateUrl: './incomes-list.component.html',
  styleUrls: ['./incomes-list.component.css']
})
export class IncomesListComponent implements OnInit {
  @Input() incomeTypes: IncomeTypeModel[];
  incomes: IncomeModel[];
  // @Input() changeDetected: boolean;

  constructor(private incomeService: IncomeService,
              private typesSettingsService: TypesSettingsService,
              private dateService: DateService) {
  }

  ngOnInit(): void {
    this.getIncomeTypes();
    this.getIncomes();
  }

  getIncomeTypes() {
    this.typesSettingsService.getIncomeTypes().subscribe(resp => {
      this.incomeTypes = resp.sort((a, b) => a.incomeTypeName.localeCompare(b.incomeTypeName));
    })
  }

  getIncomes() {
    this.incomeService.getIncomesBetweenTwoDays(this.dateService.currentMonthFirstDay(), this.dateService.currentMonthLastDay())
      .subscribe(resp => {
          //TODO surusiuoti pagal data
          this.dateService.currentMonthLastDay();
          this.incomes = resp
        }
      )
  }

  onUpdateIncome(incomeTypeName: any, incomeId: string, date: string, name: string, amount: number) {
    this.incomeService.updateIncome(this.findIncomeTypeId(incomeTypeName), incomeId, date, name, amount)
      .subscribe(resp => {
        if (resp.name === name) {
          this.getIncomes()
        }

      })
  }

  onDeleteIncome(incomeTypeName: any, incomeId: string) {
    this.incomeService.deleteIncome(this.findIncomeTypeId(incomeTypeName), incomeId).subscribe(resp => {
        if (resp.operationResult === 'SUCCESS') {
          this.getIncomes();
        } else {
          // TODO pagauti klaida
        }

      }
    )
  }

  findIncomeTypeName(incomeTypeId: string) {
    return this.incomeTypes.find(i => i.incomeTypeId === incomeTypeId).incomeTypeName;
  }

  findIncomeTypeId(name: string) {
    return this.incomeTypes.find(i => i.incomeTypeName === name).incomeTypeId;
  }

  updateInfo() {
    this.getIncomeTypes();
    this.getIncomes();
    // this.changeDetected = false;
  }

}
