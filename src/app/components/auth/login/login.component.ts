import {Component, OnInit, ViewChild} from '@angular/core';

import {NgForm} from "@angular/forms";
import {HttpClient, HttpErrorResponse, HttpHeaderResponse, HttpHeaders, HttpResponse} from "@angular/common/http";
import {map, tap} from "rxjs/operators";
import {AuthService} from "../../../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('f') loginForm: NgForm;

  message: string;

  constructor(private router: Router, private authService: AuthService) {
  }

  ngOnInit(): void {
  }

  checkLogin() {
    const email = this.loginForm.value.login.email;
    const password = this.loginForm.value.login.password

    this.authService.authenticate(email, password)
      .subscribe(resp => {
        sessionStorage.setItem('Authorization', resp.headers.get("Authorization"));
        sessionStorage.setItem('UserId', resp.headers.get("UserId"));
        sessionStorage.setItem('FirstName', resp.headers.get("FirstName"));

        if (this.authService.isUserLoggedIn()) {
          this.router.navigate(['/statistic']);
        }

      })
    // TODO pagauti klaida jeigu neprisijungia
    ;
  }


}
