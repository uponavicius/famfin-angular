import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {NgForm} from "@angular/forms";
import {UserModel} from "../../../models/user.model";
import {AuthService} from "../../../services/auth.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  @ViewChild('f') newUserForm: NgForm;

  message: string;
  showInputs = true;

  constructor(private http: HttpClient,
              private authService: AuthService) {
  }

  ngOnInit(): void {
  }

  onRegister() {
    if (this.newUserForm.value.newUser.password !== this.newUserForm.value.newUser.confirmPassword) {
      this.message = 'Passwords not match!';
    } else {
      const firstName = this.newUserForm.value.newUser.firstName;
      const lastName = this.newUserForm.value.newUser.lastName
      const email = this.newUserForm.value.newUser.email;
      const password = this.newUserForm.value.newUser.password;

      // console.log(this.newUserForm.value.newUser.firstName)
      // console.log(this.newUserForm.value.newUser.lastName)
      // console.log(this.newUserForm.value.newUser.email)
      // console.log(this.newUserForm.value.newUser.password)

      // TODO perkerlti i servisa
      this.http.post<UserModel>('http://localhost:8080/app-ws/users', {firstName, lastName, email, password})
      // this.authService.createUser(firstName, lastName, email, password)
        .subscribe(resp => {
            if (resp.userId !== null && resp.userId.length === 30) {
              this.message = 'Welcome ' + resp.firstName + ' to FamFin. Please login to email ' + resp.email + ' and confirm registration!'
              this.showInputs = false;
              this.newUserForm.reset();

            }
            return resp
          }
          , err => {
            this.message = err.error.message;
          }
        );
    }
  }

}
