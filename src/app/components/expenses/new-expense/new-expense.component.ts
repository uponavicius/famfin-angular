import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {DateService} from '../../../services/date.service';
import {ExpenseTypeModel} from "../../../models/expense-type.model";
import {TypesSettingsService} from "../../../services/types-settings.service";
import {ExpenseService} from "../../../services/expense.service";

@Component({
  selector: 'app-new-expense',
  templateUrl: './new-expense.component.html',
  styleUrls: ['./new-expense.component.css']
})
export class NewExpenseComponent implements OnInit {
  @ViewChild('f') newExpenseForm: NgForm;
  todayDate = new DateService().today();
  @Input() expenseTypes: ExpenseTypeModel[];
  // @Input() isExpenseCreated: boolean = false;
  @Output() infoChanged: EventEmitter<void> = new EventEmitter();


  constructor(private typesSettingsService: TypesSettingsService,
              private expenseService: ExpenseService) {
  }

  ngOnInit(): void {
    this.getExpenseTypes();
  }


  onSubmit(typeName: any) {
    const date = this.newExpenseForm.value.newExpenseRecord.date;
    const name = this.newExpenseForm.value.newExpenseRecord.name;
    const amount = this.newExpenseForm.value.newExpenseRecord.value;

    this.expenseService.createNewExpense(this.findExpenseTypeId(typeName), date, name, amount)
      .subscribe(resp => {
        if (resp.name === name) {
          // TODO perkauti visa lista
          // this.isExpenseCreated = true;
          this.infoChanged.emit();
        }
      })

  }

  getExpenseTypes() {
    this.typesSettingsService.getExpenseTypes().subscribe(resp => {
      this.expenseTypes = resp.sort((a, b) => a.expenseTypeName.localeCompare(b.expenseTypeName));
    })
  }

  findExpenseTypeId(name: string) {
    return this.expenseTypes.find(i => i.expenseTypeName === name).expenseTypeId;
  }

}

