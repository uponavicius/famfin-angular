import {Component, Input, OnInit} from '@angular/core';
import {ExpenseService} from "../../../services/expense.service";
import {ExpenseTypeModel} from "../../../models/expense-type.model";
import {TypesSettingsService} from "../../../services/types-settings.service";
import {ExpenseModel} from "../../../models/expense.model";
import {DateService} from "../../../services/date.service";

@Component({
  selector: 'app-expenses-list',
  templateUrl: './expenses-list.component.html',
  styleUrls: ['./expenses-list.component.css']
})
export class ExpensesListComponent implements OnInit {
  @Input() expenseTypes: ExpenseTypeModel[];
  expenses: ExpenseModel[];


  constructor(private expenseService: ExpenseService,
              private typesSettingsService: TypesSettingsService,
              private dateService: DateService) {
  }

  ngOnInit(): void {
    this.getExpenseTypes();
    this.getExpenses()
  }


  getExpenseTypes() {
    this.typesSettingsService.getExpenseTypes().subscribe(resp => {
      this.expenseTypes = resp.sort((a, b) => a.expenseTypeName.localeCompare(b.expenseTypeName));
    })
  }

  getExpenses() {
    this.expenseService.getExpensesBetweenTwoDays(this.dateService.currentMonthFirstDay(), this.dateService.currentMonthLastDay())
      .subscribe(resp => {
          //TODO surusiuoti pagal data
          this.expenses = resp;
        }
      )
  }

  onUpdateExpense(expenseTypeName: any, expenseId: string, date: string, name: string, amount: number) {
    this.expenseService.updateExpense(this.findExpenseTypeId(expenseTypeName), expenseId, date, name, amount)
      .subscribe(resp => {
        if (resp.name === name) {
          this.getExpenses()
        }
      })
  }

  onDeleteExpense(expenseTypeName: any, expenseId: string) {
    this.expenseService.deleteIncome(this.findExpenseTypeId(expenseTypeName), expenseId)
      .subscribe(resp => {
          if (resp.operationResult === 'SUCCESS') {
            this.getExpenses();
          } else {
            // TODO pagauti klaida
          }

        }
      )
  }


  findExpenseTypeName(incomeTypeId: string) {
    return this.expenseTypes.find(i => i.expenseTypeId === incomeTypeId).expenseTypeName;
  }

  findExpenseTypeId(name: string) {
    return this.expenseTypes.find(i => i.expenseTypeName === name).expenseTypeId;
  }

  updateInfo() {
    this.getExpenseTypes();
    this.getExpenses()
  }
}
