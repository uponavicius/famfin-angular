import {Component, OnInit, ViewChild} from '@angular/core';
import {CurrentMonthStatisticComponent} from "../statistics/current-month-statistic/current-month-statistic.component";
import {ExpensesListComponent} from "./expenses-list/expenses-list.component";

@Component({
  selector: 'app-expenses',
  templateUrl: './expenses.component.html',
  styleUrls: ['./expenses.component.css']
})
export class ExpensesComponent implements OnInit {
  @ViewChild(ExpensesListComponent) expensesListComponent:ExpensesListComponent;
  @ViewChild(CurrentMonthStatisticComponent) currentMonthStatisticComponent:CurrentMonthStatisticComponent;

  constructor() { }

  ngOnInit(): void {
  }

  reloadAnotherComponent() {
    this.expensesListComponent.updateInfo();
    this.currentMonthStatisticComponent.updateInfo();
  }

}
