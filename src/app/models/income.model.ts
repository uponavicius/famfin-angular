export interface IncomeModel {
  date: Date;
  name: string;
  amount: number;
  incomeId?: string;
  incomeTypeId?: string;
}
