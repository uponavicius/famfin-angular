export interface OperationStatusModel {
  operationName: string;
  operationResult: string;
}
