export interface ExpenseTypeModel {
  expenseTypeName: string;
  expenseTypeId?: string;
}
