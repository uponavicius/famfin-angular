export interface ExpenseModel {
  date: Date;
  type: string;
  name: string;
  amount: number;
  expenseId?: string;
  expenseTypeId?: string;
}
