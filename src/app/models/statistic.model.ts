export interface StatisticModel {
  currentMonthIncomes: number;
  currentMonthExpenses: number;
  currentMonthBalance: number;
  allTimeBalance: number;
  incomesByMonth?: number[];
  expensesByMonth?: number[];
  incomesStatistic?: number[];
  expenseStatistic?: number[];
}
